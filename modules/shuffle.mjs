// Implement the Knuth shuffle algorithm (aka Fisher–Yates shuffle) to shuffle an array in-place
function knuth_shuffle(arr) {
    var n = arr.length,
            r,
            temp;
    while (n > 1) {
        r = Math.floor(n * Math.random());
        n -= 1;
        temp = arr[n];
        arr[n] = arr[r];
        arr[r] = temp;
    }
    return arr;
}

/**
 * Shuffles the array `a` without moving the element which are set to true in `f`
 * @param {Array} `a` an array containing the items' value
 * @param {Object} `f` an array containing the items' state
 *
 * From https://codereview.stackexchange.com/q/196493/18865
 */
function shuffle(a, f = []) {
    var list = a.reduce((acc, e, i) => {
        if(!f[i]) {
            acc.pos.push(i);
            acc.unfixed.push(e);
        };
        return acc;
    }, { pos: [], unfixed: [] });

    list.pos = knuth_shuffle(list.pos);

    return a.map((e, i) => {
        return f[i] ? e : list.unfixed[list.pos.indexOf(i)]
    })
}

export { shuffle };
