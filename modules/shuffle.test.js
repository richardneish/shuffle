import { shuffle } from './shuffle.mjs'

test('shuffles an array to get the same elements in a different order', () => {
  const actual = expect(shuffle(['a','b','c']));
  actual.not.toEqual(['a','b','c']);
  actual.toContain('c');
  actual.toContain('a');
  actual.toContain('b');
});

test('shuffles an array with all elements pinned to get exactly the same array', () => {
  expect(shuffle(['a', 'b', 'c'], [true, true, true])).toEqual(['a','b','c']);
})
