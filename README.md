Shuffle
=======

Tiny HTML / Vue. / JavaScript app to shuffle letters - useful for manually solving anagrams.

Running
=====
The application can run directly from the working copy without building or packaging.

Testing
=====
Jest is used for testing.  Run 'npm test'
