import { shuffle } from './modules/shuffle.mjs'

var shuffleApp = new Vue({
  el: '#app-shuffle',
  data: {
    letterObjects: []
  },
  computed: {
    letters: {
      get: function() {
        return this.letterObjects
          .map(item => item.value)
          .join('');
      },
      set: function(newValue) {
        this.letterObjects = Array.from(newValue)
          .map(c => { return { 'value': c }; })
      }
    }
  },
  methods: {
    doShuffle: function(event) {
      var lettersToShuffle = this.letterObjects
        .filter(item => !item.locked)
      lettersToShuffle = shuffle(lettersToShuffle);
      for (var i = 0; i < this.letterObjects.length; i++) {
        if (!this.letterObjects[i].locked) {
          this.letterObjects.splice(i, 1, lettersToShuffle.shift());
        }
      }
    },
    toggleLock: function(index) {
      var newValue = this.letterObjects[index];
      newValue.locked = !newValue.locked;
      this.letterObjects.splice(index, 1, newValue);
    },
    moveToInput: function(index, event) {
      var nextIndex;
      var letterInput = event.target.value;
      if (letterInput === '') {
        // Remove this letter
        nextIndex = index > 0 ? index-1 : 0;
        this.letterObjects.splice(index, 1);
      } else {
        nextIndex = index+1;
        if (nextIndex >= this.letterObjects.length + 1) {
          console.log("Attempt to move past end of input - not implemented");
          return;
        }
      }
      document.querySelector('#letter-input-' + nextIndex).focus();
    },
    appendLetter: function(event) {
        var letterToAppend = event.target.value;
        event.target.value = '';
        this.letterObjects.push({value: letterToAppend});
        return;    
    },
    checkBackspace: function(event) {
      // TODO: Handle arrow keys as well
      if (event.key === "Backspace" && this.letterObjects.length > 0) {
        this.letterObjects.splice(this.letterObjects.length-1, 1);
      }
    }
  },
  directives: {
    focus: {
      inserted: function (el) {
        el.focus()
      }
    }
  }
})
